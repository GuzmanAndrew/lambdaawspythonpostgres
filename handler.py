import pg8000

endpoint = 'bmed-usuario-db.csro2nol6a13.us-east-1.rds.amazonaws.com'
username = 'postgres'
password = 'Andrewseigokan9*'
database = 'postgres'
port = '5432'

conection = pg8000.connect(user=username,
                            password=password,
                            host=endpoint,
                            port=port,
                            database=database)

""" Para probar en local se debe de quitar los parametros "event" y "context" de la funcion
y descomentar el llamado de la funcion """
def lambda_handler(event, context):
    cursor = conection.cursor()

    """ sql = 'SELECT * FROM frecuencia_cardiaca where id_paciente = 18'
    cursor.execute(sql)
    registers = cursor.fetchall()
    for register in registers:
        print('{0} {1} {2} {3} {4}'.format(register[0], register[1], register[2], register[3], register[4])) """

    """ sql = 'INSERT INTO frecuencia_cardiaca(fecha, hora, frecuencia, id_paciente) VALUES (%s, %s, %s, %s)'
    fecha = '19/02/2023'
    hora = '05:36'
    frecuencia = '130'
    id_paciente = 16
    data = (fecha, hora, frecuencia, id_paciente)
    cursor.execute(sql, data)
    conection.commit()
    register_insert = cursor.rowcount
    print(f'registro insertado: {register_insert}') """

    sql = 'update frecuencia_cardiaca set frecuencia = 15 where id_paciente = 18'
    cursor.execute(sql)
    conection.commit()
    register_update = cursor.rowcount
    print(f'registro actualizado: {register_update}')

    cursor.close()
    conection.close()

# lambda_handler()