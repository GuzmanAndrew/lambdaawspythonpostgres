
# This file was generated by 'versioneer.py' (0.19) from
# revision-control system data, or from the parent directory name of an
# unpacked source archive. Distribution tarballs contain a pre-generated copy
# of this file.

import json

version_json = '''
{
 "date": "2022-04-23T14:58:40+0100",
 "dirty": false,
 "error": null,
 "full-revisionid": "794420aa5f870b5163eca79bd21ac98498f4a65c",
 "version": "1.26.1"
}
'''  # END VERSION_JSON


def get_versions():
    return json.loads(version_json)
